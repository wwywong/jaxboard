# JaxBoard

A typewriter-style, bare-bones MathJax previewer with some limited text formatting capabilities, with very generous output space and large fonts, and few distracting elements. Intended to be used for teaching (either in person for large lecture halls, via projectors; or via Zoom via screen share), or as a quick scratch board for mathematical discussions and brainstorming. 

## Change log

- Version 4.3 update 2022-2-9:
   - Paragraphs are now highlighted by mouse hover; should improve presentation focus. 
- Version 4.2 released 2022-1-5: UI improvements
   - When within Mathmode, the commands \\) and \\] now allows exiting from mathmode  (note: doesn't have to be at end of expression)
   - When within Mathmode, typing two backslashes in a row will be auto-replaced by \\newline.
   - When entering an existing mathmode node, the current text is now selected
   - Added some default MJ macros
- Version 4.1 released 2022-1-4: multiple changes
   - Added load/save functionality to the new version
   - Repackaged as PWA
   - added function to auto-prune empty mathwraps
   - added javascript comments to code
- Version 4 released 2022-1-4: better UI for math editing.
- Version 3 released 2022-1-2: rewrote from scratch. Old version gets very laggy after use. This one should be more responsive due to much fewer JS calls. 
- Version 2.1 released 2021-1-7: fixed MathJax bug with save states. Moved preview box to part of the display area to simplify usage. Updated help files.   
- Version 2 released 2021-1-6: significant changes made to how MathJax calls are handled to improve efficiency. Now it is less likely to freeze up my laptop with large inputs. JaxBoard is also now packaged as a progressive web app, and can be used offline. To facilitate offline use functions are added to allow board states to be saved and loaded.  

## Credits

Versions 1 and 2 were forks of MathJax Scratchpad by Sean Droms
https://mas.lvc.edu/droms/jax/
http://seandroms.com/

Version 3 on is based on entirely new code base using HTML5's `contenteditable` attribute. 

## License

All material in this repository, except noted below, are released **as is** into Public Domain. Feel free to make your own copy for your own use. 

Exceptions:

- Site logo (included as a `png` file in this repository) which is © Willie WY Wong. 
- Legacy code in `jb2.html`, portions of which are © Sean Droms. 
